# This repository has been moved

The new location is [`debian/52/master` branch in `mozjs` repo](https://salsa.debian.org/gnome-team/mozjs/tree/debian/52/master).

If you had cloned this repo already (now or in the past), you can just:

    git remote set-url origin https://salsa.debian.org/gnome-team/mozjs.git
    git fetch origin
    git checkout debian/52/master
